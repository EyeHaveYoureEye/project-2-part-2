package tech.pm.edu.simlewallet.domain.model.enums;

public enum Type {

  BET(1),
  WIN(2);

  int value;

  Type(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }


}
