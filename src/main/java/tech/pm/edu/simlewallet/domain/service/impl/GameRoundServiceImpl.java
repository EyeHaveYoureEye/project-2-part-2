package tech.pm.edu.simlewallet.domain.service.impl;

import tech.pm.edu.simlewallet.db.model.*;
import tech.pm.edu.simlewallet.db.service.*;
import tech.pm.edu.simlewallet.domain.mapper.*;
import tech.pm.edu.simlewallet.domain.model.*;
import tech.pm.edu.simlewallet.domain.service.*;

import java.util.*;

public class GameRoundServiceImpl implements GameRoundService {

  private final GameRoundEntityService gameRoundEntityService;

  public GameRoundServiceImpl(GameRoundEntityService gameRoundEntityService) {
    this.gameRoundEntityService = gameRoundEntityService;

  }

  @Override
  public Optional<GameRound> getGameRoundByRoundId(String gameRoundId) {
    Optional<GameRoundEntity> optionalGameRoundEntity = gameRoundEntityService.getGameRoundByRoundId(gameRoundId);
    if (optionalGameRoundEntity.isPresent()) {
      GameRoundEntity gameRoundEntity = optionalGameRoundEntity.get();
      return Optional.of(GameRoundMapper.gameRoundEntityToGameRound((gameRoundEntity)));
    } else {
      return Optional.empty();
    }
  }


}
