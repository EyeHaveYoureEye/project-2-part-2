package tech.pm.edu.simlewallet.domain.model;

import lombok.*;
import tech.pm.edu.simlewallet.domain.model.enums.*;

@Value
public class Game {

  String gameId;
  Status status;
  String gameName;


}
