package tech.pm.edu.simlewallet.domain.model;

import lombok.*;
import tech.pm.edu.simlewallet.domain.model.enums.*;

import java.time.*;

@Value
public class GameTransaction {

  String walletTxId;
  Integer txId;
  String roundId;
  Double amount;
  Type type;
  Instant timestamp;


}
