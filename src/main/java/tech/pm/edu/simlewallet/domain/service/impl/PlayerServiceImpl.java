package tech.pm.edu.simlewallet.domain.service.impl;

import tech.pm.edu.simlewallet.db.model.*;
import tech.pm.edu.simlewallet.db.service.*;
import tech.pm.edu.simlewallet.domain.mapper.*;
import tech.pm.edu.simlewallet.domain.model.*;
import tech.pm.edu.simlewallet.domain.model.enums.*;
import tech.pm.edu.simlewallet.domain.service.*;

import java.util.*;

public class PlayerServiceImpl implements PlayerService {

  private final PlayerEntityService playerEntityService;

  public PlayerServiceImpl(PlayerEntityService playerEntityService) {
    this.playerEntityService = playerEntityService;
  }

  @Override
  public Optional<Player> getPlayerById(Integer playerId) {
    Optional<PlayerEntity> optionalPlayerEntity = playerEntityService.getPlayerById(playerId);
    if (optionalPlayerEntity.isPresent()) {
      PlayerEntity playerEntity = optionalPlayerEntity.get();
      return Optional.of(PlayerMapper.playerEntityToPlayer(playerEntity));
    } else {
      return Optional.empty();
    }
  }

  @Override
  public boolean updateBalance(Integer playerId, Double newBalance, Type type, GameRound gameRound, GameTransaction gameTransaction) {
    return playerEntityService.updateBalance(playerId, newBalance, type, gameRound, gameTransaction);
  }


}
