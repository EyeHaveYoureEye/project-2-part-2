package tech.pm.edu.simlewallet.domain.model;

import lombok.*;
import tech.pm.edu.simlewallet.domain.model.enums.*;

import java.time.*;

@Value
public class Player {

  Integer playerId;
  Instant registrationDate;
  Status status;
  Integer countryId;
  Double balance;
  String currency;


}
