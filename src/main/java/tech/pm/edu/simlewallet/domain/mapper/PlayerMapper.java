package tech.pm.edu.simlewallet.domain.mapper;

import tech.pm.edu.simlewallet.db.model.*;
import tech.pm.edu.simlewallet.domain.model.*;

public class PlayerMapper {

  private PlayerMapper() {
  }

  public static Player playerEntityToPlayer(PlayerEntity playerEntity) {
    if (playerEntity != null) {
      return new Player(
        playerEntity.getPlayerId(),
        playerEntity.getRegistrationDate(),
        playerEntity.getStatus(),
        playerEntity.getCountryId(),
        playerEntity.getBalance(),
        playerEntity.getCurrency()
      );
    } else {
      return null;
    }
  }


}
