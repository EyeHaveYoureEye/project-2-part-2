package tech.pm.edu.simlewallet.domain.model;

import lombok.*;

import java.time.*;

@Value
public class GameRound {

  String gameRoundId;
  Integer gameRound;
  Integer playerId;
  String gameId;
  Instant startTime;
  Instant endTime;
  Boolean isFinished;


}
