package tech.pm.edu.simlewallet.domain.service.impl;

import tech.pm.edu.simlewallet.db.model.*;
import tech.pm.edu.simlewallet.db.service.*;
import tech.pm.edu.simlewallet.domain.mapper.*;
import tech.pm.edu.simlewallet.domain.model.*;
import tech.pm.edu.simlewallet.domain.service.*;

import java.util.*;

public class GameServiceImpl implements GameService {

  private final GameEntityService gameEntityService;

  public GameServiceImpl(GameEntityService gameEntityService) {
    this.gameEntityService = gameEntityService;
  }

  @Override
  public Optional<Game> getGameById(String gameId) {
    Optional<GameEntity> optionalGameEntity = gameEntityService.getGameById(gameId);
    if (optionalGameEntity.isPresent()) {
      GameEntity gameEntity = optionalGameEntity.get();
      return Optional.of(GameMapper.gameEntityToGame(gameEntity));
    } else {
      return Optional.empty();
    }
  }


}
