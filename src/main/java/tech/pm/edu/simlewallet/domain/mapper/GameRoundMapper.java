package tech.pm.edu.simlewallet.domain.mapper;

import tech.pm.edu.simlewallet.db.model.*;
import tech.pm.edu.simlewallet.domain.model.*;

public class GameRoundMapper {

  private GameRoundMapper() {
  }

  public static GameRound gameRoundEntityToGameRound(GameRoundEntity gameRoundEntity) {
    if (gameRoundEntity != null) {
      return new GameRound(
        gameRoundEntity.getGameRoundId(),
        gameRoundEntity.getGameRound(),
        gameRoundEntity.getPlayerId(),
        gameRoundEntity.getGameId(),
        gameRoundEntity.getStartTime(),
        gameRoundEntity.getEndTime(),
        gameRoundEntity.getIsFinished()
      );
    } else {
      return null;
    }
  }


}
