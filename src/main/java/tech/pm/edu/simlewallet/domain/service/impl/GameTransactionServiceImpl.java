package tech.pm.edu.simlewallet.domain.service.impl;

import tech.pm.edu.simlewallet.db.model.*;
import tech.pm.edu.simlewallet.db.service.*;
import tech.pm.edu.simlewallet.domain.mapper.*;
import tech.pm.edu.simlewallet.domain.model.*;
import tech.pm.edu.simlewallet.domain.service.*;

import java.util.*;

public class GameTransactionServiceImpl implements GameTransactionService {

  private final GameTransactionEntityService gameTransactionEntityService;

  public GameTransactionServiceImpl(GameTransactionEntityService gameTransactionEntityService) {
    this.gameTransactionEntityService = gameTransactionEntityService;

  }

  @Override
  public Optional<GameTransaction> getGameTransactionById(String gameRoundId) {
    Optional<GameTransactionEntity> optionalGameTransactionEntity = gameTransactionEntityService.getGameTransactionById(gameRoundId);
    if (optionalGameTransactionEntity.isPresent()) {
      GameTransactionEntity gameTransactionEntity = optionalGameTransactionEntity.get();
      return Optional.of(GameTransactionMapper.gameTransactionEntityToGameTransaction(gameTransactionEntity));
    } else {
      return Optional.empty();
    }
  }

  @Override
  public Optional<List<GameTransaction>> getListOfGameTransactionByRoundId(String roundId) {
    Optional<List<GameTransactionEntity>> optionalGameTransactionEntityList =
      gameTransactionEntityService.getListOfGameTransactionEntityByRoundId(roundId);
    if (optionalGameTransactionEntityList.isPresent()) {
      List<GameTransactionEntity> gameTransactionEntities = new ArrayList<>(optionalGameTransactionEntityList.get());
      List<GameTransaction> gameTransactions = new ArrayList<>();
      for (GameTransactionEntity gameTransactionEntity : gameTransactionEntities) {
        gameTransactions.add(GameTransactionMapper.gameTransactionEntityToGameTransaction(gameTransactionEntity));
      }
      return Optional.of(gameTransactions);
    } else {
      return Optional.empty();
    }
  }


}
