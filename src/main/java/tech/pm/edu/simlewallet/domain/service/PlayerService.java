package tech.pm.edu.simlewallet.domain.service;

import tech.pm.edu.simlewallet.domain.model.*;
import tech.pm.edu.simlewallet.domain.model.enums.*;

import java.util.*;

public interface PlayerService {

  Optional<Player> getPlayerById(Integer playerId);

  boolean updateBalance(Integer playerId, Double newBalance, Type type, GameRound gameRound, GameTransaction gameTransaction);


}
