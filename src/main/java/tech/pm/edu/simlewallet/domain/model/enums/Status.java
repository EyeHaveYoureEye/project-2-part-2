package tech.pm.edu.simlewallet.domain.model.enums;

public enum Status {
  ACTIVE(1),
  INACTIVE(2);

  int value;

  Status(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }


}
