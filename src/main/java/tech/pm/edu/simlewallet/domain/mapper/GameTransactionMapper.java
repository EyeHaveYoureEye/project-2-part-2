package tech.pm.edu.simlewallet.domain.mapper;

import tech.pm.edu.simlewallet.db.model.*;
import tech.pm.edu.simlewallet.domain.model.*;


public class GameTransactionMapper {

  private GameTransactionMapper() {
  }

  public static GameTransaction gameTransactionEntityToGameTransaction(GameTransactionEntity gameTransactionEntity) {
    if (gameTransactionEntity != null) {
      return new GameTransaction(
        gameTransactionEntity.getWalletTxId(),
        gameTransactionEntity.getTxId(),
        gameTransactionEntity.getRoundId(),
        gameTransactionEntity.getAmount(),
        gameTransactionEntity.getType(),
        gameTransactionEntity.getTimestamp()
      );
    } else {
      return null;
    }
  }


}
