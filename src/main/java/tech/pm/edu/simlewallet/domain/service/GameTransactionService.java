package tech.pm.edu.simlewallet.domain.service;

import tech.pm.edu.simlewallet.domain.model.*;

import java.util.*;

public interface GameTransactionService {

  Optional<GameTransaction> getGameTransactionById(String gameRoundId);

  Optional<List<GameTransaction>> getListOfGameTransactionByRoundId(String roundId);


}
