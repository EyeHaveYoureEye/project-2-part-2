package tech.pm.edu.simlewallet.domain.service;

import tech.pm.edu.simlewallet.domain.model.*;

import java.util.*;

public interface GameService {

  Optional<Game> getGameById(String gameId);


}
