package tech.pm.edu.simlewallet.domain.mapper;

import tech.pm.edu.simlewallet.db.model.*;
import tech.pm.edu.simlewallet.domain.model.*;

public class GameMapper {

  private GameMapper() {
  }

  public static Game gameEntityToGame(GameEntity gameEntity) {
    if (gameEntity != null) {
      return new Game(
        gameEntity.getGameId(),
        gameEntity.getStatus(),
        gameEntity.getGameName()
      );
    } else {
      return null;
    }
  }


}
