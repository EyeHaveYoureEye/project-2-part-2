package tech.pm.edu.simlewallet.web.exceptions;

public enum ExceptionCode implements ErrorCodeConstant {

  UNEXPECTED_ERROR(CODE_801, "Unexpected error"),
  SECURITY_KEY_MISMATCH(CODE_810, "Security key mismatch"),
  WRONG_PLAYER_ID(CODE_820, "Wrong player Id."),
  A_PLAYER_IS_BLOCKED(CODE_821, "A player is blocked"),
  INSUFFICIENT_FUNDS(CODE_831, "Insufficient funds"),
  BET_ALREADY_SETTLED(CODE_833, "Bet already settled"),
  BET_WITH_SPECIFIED_ID_NOT_FOUND(CODE_840, "Bet with specified ID not found");

  private final int id;
  private final String message;

  ExceptionCode(int id, String message) {
    this.id = id;
    this.message = message;
  }

  public int getId() {
    return this.id;
  }

  public String getMessage() {
    return this.message;
  }


}
