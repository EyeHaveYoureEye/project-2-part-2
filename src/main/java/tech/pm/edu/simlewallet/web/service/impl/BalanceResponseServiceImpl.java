package tech.pm.edu.simlewallet.web.service.impl;

import org.apache.log4j.*;
import tech.pm.edu.simlewallet.domain.model.*;
import tech.pm.edu.simlewallet.domain.service.*;
import tech.pm.edu.simlewallet.web.exceptions.*;
import tech.pm.edu.simlewallet.web.model.*;
import tech.pm.edu.simlewallet.web.model.enams.*;
import tech.pm.edu.simlewallet.web.service.*;

import java.math.*;
import java.util.*;

public class BalanceResponseServiceImpl implements BalanceResponseService {

  private static final Logger LOGGER = Logger.getLogger(BalanceResponseServiceImpl.class);
  private final PlayerService playerService;

  public BalanceResponseServiceImpl(PlayerService playerService) {
    this.playerService = playerService;
  }

  @Override
  public BalanceResponse getBalanceResponse(String playerId) {
    Optional<Player> optionalPlayer = playerService.getPlayerById(Integer.parseInt(playerId));

    if (optionalPlayer.isPresent()) {
      Player player = optionalPlayer.get();

      BalanceDataResponse balanceDataResponse = new BalanceDataResponse();
      balanceDataResponse.setPlayerId(player.getPlayerId());
      balanceDataResponse.setBalance(BigDecimal.valueOf(player.getBalance()));
      balanceDataResponse.setCurrency(player.getCurrency());

      BalanceResponse balanceResponse = new BalanceResponse();
      balanceResponse.setStatus(ResponseStatus.SUCCESS.toString());
      balanceResponse.setBalanceDataResponse(balanceDataResponse);

      LOGGER.info(balanceResponse);

      return balanceResponse;
    } else {
      WebException exception = new WebException(ExceptionCode.WRONG_PLAYER_ID);

      LOGGER.error(exception.getMessage());

      throw exception;
    }
  }


}
