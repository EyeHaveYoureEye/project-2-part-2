package tech.pm.edu.simlewallet.web.controller;

import com.fasterxml.jackson.databind.*;
import tech.pm.edu.simlewallet.web.context.*;
import tech.pm.edu.simlewallet.web.exceptions.*;
import tech.pm.edu.simlewallet.web.exceptions.model.*;
import tech.pm.edu.simlewallet.web.exceptions.service.*;
import tech.pm.edu.simlewallet.web.model.*;
import tech.pm.edu.simlewallet.web.service.*;

import javax.servlet.annotation.*;
import javax.servlet.http.*;
import java.io.*;

@WebServlet("/api/game/balance")
public class BalanceServlet extends HttpServlet {

  ObjectMapper objectMapper = new ObjectMapper();
  BalanceResponseService balanceResponseService;
  ErrorResponseService errorResponseService;

  @Override
  public void init() {
    balanceResponseService = ApplicationContextInjector.getBalanceResponseService();
    errorResponseService = ApplicationContextInjector.getErrorResponseService();
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    resp.setContentType("application/json;charset=UTF-8");
    String playerId = req.getParameter("playerId");
    String response;
    try {
      BalanceResponse balanceResponse = balanceResponseService.getBalanceResponse(playerId);
      response = objectMapper.writeValueAsString(balanceResponse);
      resp.getWriter().write(response);
    } catch (WebException webException) {
      ErrorResponseModel errorResponseModel = errorResponseService.getErrorResponseModel(webException);
      response = objectMapper.writeValueAsString(errorResponseModel);
      resp.getWriter().write(response);
    }
  }


}
