package tech.pm.edu.simlewallet.web.exceptions.model;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import java.util.*;

@Data
public class ErrorResponseModel {

  private String status;
  private String message;
  @JsonProperty("errors")
  private List<ErrorDetailsModel> errors = new LinkedList<>();

  public void addErrorDetailsToErrorsList(ErrorDetailsModel errorDetailsModel) {
    errors.add(errorDetailsModel);
  }


}
