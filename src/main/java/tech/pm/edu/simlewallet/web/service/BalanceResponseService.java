package tech.pm.edu.simlewallet.web.service;

import tech.pm.edu.simlewallet.web.model.*;

public interface BalanceResponseService {

  BalanceResponse getBalanceResponse(String playerId);


}
