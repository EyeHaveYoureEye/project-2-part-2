package tech.pm.edu.simlewallet.web.exceptions.service;

import tech.pm.edu.simlewallet.web.exceptions.*;
import tech.pm.edu.simlewallet.web.exceptions.model.*;

public interface ErrorResponseService {

  ErrorResponseModel getErrorResponseModel(WebException webException);


}
