package tech.pm.edu.simlewallet.web.exceptions.model;

import lombok.*;

@Data
public class ErrorDetailsModel {

  private int code;
  private String message;


}
