package tech.pm.edu.simlewallet.web.service.impl;

import org.apache.log4j.*;
import tech.pm.edu.simlewallet.domain.model.*;
import tech.pm.edu.simlewallet.domain.model.enums.*;
import tech.pm.edu.simlewallet.domain.service.*;
import tech.pm.edu.simlewallet.web.exceptions.*;
import tech.pm.edu.simlewallet.web.model.*;
import tech.pm.edu.simlewallet.web.model.enams.*;
import tech.pm.edu.simlewallet.web.service.*;
import tech.pm.edu.simlewallet.web.service.util.*;

import java.math.*;
import java.time.*;

public class WinResponseServiceImpl implements WinResponseService {

  private static final Logger LOGGER = Logger.getLogger(WinResponseServiceImpl.class);
  private final PlayerService playerService;
  private final GameRoundService gameRoundService;
  private final GameService gameService;
  private final GameTransactionService gameTransactionService;

  public WinResponseServiceImpl(PlayerService playerService,
                                GameRoundService gameRoundService,
                                GameService gameService,
                                GameTransactionService gameTransactionService) {
    this.playerService = playerService;
    this.gameRoundService = gameRoundService;
    this.gameService = gameService;
    this.gameTransactionService = gameTransactionService;
  }

  @Override
  public WinResponse getWinResponse(WinRequest winRequest) {
    Integer playerId = winRequest.getPlayerId();
    String gameId = winRequest.getTransaction().getGameId();
    Integer round = winRequest.getTransaction().getRoundId();
    Integer txId = winRequest.getTransaction().getTxId();
    String walletTxId = winRequest.getTransaction().getWalletTxId();
    BigDecimal amount = winRequest.getAmount();
    String currency = winRequest.getCurrency();
    String gameRoundId = String.format("%s%s%s", round, playerId, gameId);

    Player player = RequestValidator.validatePlayer(playerId, playerService);
    RequestValidator.validateGame(gameId, gameService);
    Double newBalance = player.getBalance() + amount.doubleValue();
    GameRound gameRound = RequestValidator.validateGameRound(gameRoundId, gameRoundService);
    RequestValidator.validateBetTxIdAndWinTxId(gameRoundId, txId, gameTransactionService);

    GameTransaction gameTransaction = new GameTransaction(walletTxId, txId, gameRoundId, amount.doubleValue(), Type.WIN, Instant.now());

    if (player.getCurrency().equals(currency) && playerService.updateBalance(playerId, newBalance, Type.WIN, gameRound, gameTransaction)) {
      WinResponse winResponse = createWinResponse(playerId, newBalance,
        currency, gameTransaction.getTxId(), gameTransaction.getWalletTxId());

      LOGGER.info(String.format("Successfully created response:%n%-20s%n%-20s", gameTransaction, winResponse));

      return winResponse;
    }

    WebException exception = new WebException(ExceptionCode.UNEXPECTED_ERROR);
    LOGGER.error(exception.getErrorMessage() + " Request currency mismatch");
    throw exception;
  }

  private WinResponse createWinResponse(Integer playerId, Double balance, String currency,
                                        Integer originalTxId, String walletTxId) {
    WinDataResponse winDataResponse = new WinDataResponse();
    winDataResponse.setPlayerId(playerId);
    winDataResponse.setBalance(BigDecimal.valueOf(balance));
    winDataResponse.setCurrency(currency);
    winDataResponse.setOriginalTxId(originalTxId);
    winDataResponse.setWalletTxId(walletTxId);
    WinResponse winResponse = new WinResponse();
    winResponse.setStatus(ResponseStatus.SUCCESS.toString());
    winResponse.setData(winDataResponse);
    return winResponse;
  }


}
