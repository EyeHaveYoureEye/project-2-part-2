package tech.pm.edu.simlewallet.web.model;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

@Data
public class WinResponse {

  private String status;
  @JsonProperty("data")
  private WinDataResponse data;


}
