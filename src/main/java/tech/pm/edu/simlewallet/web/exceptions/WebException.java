package tech.pm.edu.simlewallet.web.exceptions;

public class WebException extends RuntimeException {

  private final int errorCode;
  private final String errorMessage;

  public WebException(ExceptionCode code) {
    this.errorMessage = code.getMessage();
    this.errorCode = code.getId();
  }

  public int getErrorCode() {
    return errorCode;
  }

  public String getErrorMessage() {
    return errorMessage;
  }


}