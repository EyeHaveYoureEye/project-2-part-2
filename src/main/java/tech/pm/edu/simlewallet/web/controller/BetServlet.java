package tech.pm.edu.simlewallet.web.controller;

import com.fasterxml.jackson.databind.*;
import tech.pm.edu.simlewallet.web.context.*;
import tech.pm.edu.simlewallet.web.exceptions.*;
import tech.pm.edu.simlewallet.web.exceptions.model.*;
import tech.pm.edu.simlewallet.web.exceptions.service.*;
import tech.pm.edu.simlewallet.web.model.*;
import tech.pm.edu.simlewallet.web.service.*;

import javax.servlet.annotation.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.stream.*;

@WebServlet("/api/game/bet")
public class BetServlet extends HttpServlet {

  ObjectMapper objectMapper = new ObjectMapper();
  BetResponseService betResponseService;
  ErrorResponseService errorResponseService;

  @Override
  public void init() {
    betResponseService = ApplicationContextInjector.getBetResponseService();
    errorResponseService = ApplicationContextInjector.getErrorResponseService();
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    String betRequestData = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
    BetRequest betRequest = objectMapper.readValue(betRequestData, BetRequest.class);
    String response;
    try {
      BetResponse betResponse = betResponseService.getBetResponse(betRequest);
      response = objectMapper.writeValueAsString(betResponse);
      resp.getWriter().write(response);
    } catch (WebException webException) {
      ErrorResponseModel errorResponseModel = errorResponseService.getErrorResponseModel(webException);
      response = objectMapper.writeValueAsString(errorResponseModel);
      resp.getWriter().write(response);
    }
  }


}
