package tech.pm.edu.simlewallet.web.service.util;

import org.apache.log4j.*;
import tech.pm.edu.simlewallet.domain.model.*;
import tech.pm.edu.simlewallet.domain.service.*;
import tech.pm.edu.simlewallet.web.exceptions.*;

import java.util.*;

public class RequestValidator {

  private static final Logger LOGGER = Logger.getLogger(RequestValidator.class);

  private RequestValidator() {
  }

  public static Player validatePlayer(Integer playerId, PlayerService playerService) {
    Optional<Player> optionalPlayer = playerService.getPlayerById(playerId);
    Player player;
    if (optionalPlayer.isEmpty()) {
      WebException exception = new WebException(ExceptionCode.WRONG_PLAYER_ID);
      LOGGER.error(exception.getErrorMessage());
      throw exception;
    } else {
      player = optionalPlayer.get();
      if (player.getStatus().getValue() == 2) {
        WebException exception = new WebException(ExceptionCode.A_PLAYER_IS_BLOCKED);
        LOGGER.error(exception.getErrorMessage());
        throw exception;
      } else {
        return player;
      }
    }
  }

  public static void validateGame(String gameId, GameService gameService) {
    Optional<Game> optionalGame = gameService.getGameById(gameId);
    if (optionalGame.isEmpty()) {
      WebException exception = new WebException(ExceptionCode.UNEXPECTED_ERROR);
      LOGGER.error(exception.getErrorMessage());
      throw exception;
    } else {
      Game game = optionalGame.get();
      if (game.getStatus().getValue() == 2) {
        WebException exception = new WebException(ExceptionCode.UNEXPECTED_ERROR);
        LOGGER.error(exception.getErrorMessage());
        throw exception;
      }
    }
  }

  public static Double validateGameTransaction(Double balance, Double amount, String playerCurrency, String requestCurrency) {
    if (!playerCurrency.equals(requestCurrency)) {
      WebException exception = new WebException(ExceptionCode.UNEXPECTED_ERROR);
      LOGGER.error(exception.getErrorMessage() + " - currency mismatch");
      throw exception;
    }
    double newBalance = balance - amount;
    if (newBalance >= 0) {
      return newBalance;
    } else {
      WebException exception = new WebException(ExceptionCode.INSUFFICIENT_FUNDS);
      LOGGER.error(exception.getErrorMessage());
      throw exception;
    }
  }

  public static GameRound validateGameRound(String gameRoundId, GameRoundService gameRoundService) {
    Optional<GameRound> optionalGameRound = gameRoundService.getGameRoundByRoundId(gameRoundId);
    if (optionalGameRound.isEmpty()) {
      WebException exception = new WebException(ExceptionCode.BET_WITH_SPECIFIED_ID_NOT_FOUND);
      LOGGER.error(exception.getErrorMessage());
      throw exception;
    } else {
      return optionalGameRound.get();
    }
  }

  public static void validateGameRoundExists(String gameRoundId, GameRoundService gameRoundService) {
    Optional<GameRound> optionalGameRound = gameRoundService.getGameRoundByRoundId(gameRoundId);
    if (optionalGameRound.isPresent()) {
      WebException exception = new WebException(ExceptionCode.BET_ALREADY_SETTLED);
      LOGGER.error(exception.getErrorMessage());
      throw exception;
    }
  }

  public static void validateBetTxIdAndWinTxId(String roundId, Integer winTxId, GameTransactionService gameTransactionService) {
    Optional<List<GameTransaction>> optionalGameTransactionList = gameTransactionService.getListOfGameTransactionByRoundId(roundId);
    if (optionalGameTransactionList.isPresent()) {
      List<GameTransaction> gameTransactions = optionalGameTransactionList.get();
      for (GameTransaction gameTransaction : gameTransactions) {
        if (gameTransaction.getTxId().equals(winTxId)) {
          WebException exception = new WebException(ExceptionCode.UNEXPECTED_ERROR);
          LOGGER.error(exception.getErrorMessage() + " - Request win TxId is not unique!");
          throw exception;
        }
      }
    }
  }


}
