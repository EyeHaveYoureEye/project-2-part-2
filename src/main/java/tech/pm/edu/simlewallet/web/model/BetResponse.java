package tech.pm.edu.simlewallet.web.model;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

@Data
public class BetResponse {

  private String status;
  @JsonProperty("data")
  private BetDataResponse data;


}
