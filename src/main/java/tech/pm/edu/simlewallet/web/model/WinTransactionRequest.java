package tech.pm.edu.simlewallet.web.model;

import lombok.*;

@Data
public class WinTransactionRequest {

  private String gameId;
  private Integer roundId;
  private Integer txId;
  private String walletTxId;


}
