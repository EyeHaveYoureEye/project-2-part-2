package tech.pm.edu.simlewallet.web.service.impl;

import org.apache.log4j.*;
import tech.pm.edu.simlewallet.domain.model.*;
import tech.pm.edu.simlewallet.domain.model.enums.*;
import tech.pm.edu.simlewallet.domain.service.*;
import tech.pm.edu.simlewallet.web.exceptions.*;
import tech.pm.edu.simlewallet.web.model.*;
import tech.pm.edu.simlewallet.web.model.enams.*;
import tech.pm.edu.simlewallet.web.service.*;
import tech.pm.edu.simlewallet.web.service.util.*;

import java.math.*;
import java.time.*;

public class BetResponseServiceImpl implements BetResponseService {

  private static final Logger LOGGER = Logger.getLogger(BetResponseServiceImpl.class);
  private final PlayerService playerService;
  private final GameService gameService;
  private final GameRoundService gameRoundService;

  public BetResponseServiceImpl(PlayerService playerService, GameService gameService, GameRoundService gameRoundService) {
    this.playerService = playerService;
    this.gameService = gameService;
    this.gameRoundService = gameRoundService;
  }

  @Override
  public BetResponse getBetResponse(BetRequest betRequest) {
    Integer playerId = betRequest.getPlayerId();
    String gameId = betRequest.getTransaction().getGameId();
    Integer round = betRequest.getTransaction().getRoundId();
    Integer txId = betRequest.getTransaction().getTxId();
    String walletTxId = betRequest.getTransaction().getWalletTxId();
    String currency = betRequest.getCurrency();
    BigDecimal amount = betRequest.getAmount();
    String gameRoundId = String.format("%s%s%s", round, playerId, gameId);

    Player player = RequestValidator.validatePlayer(playerId, playerService);
    RequestValidator.validateGame(gameId, gameService);
    Double newBalance = RequestValidator.validateGameTransaction(player.getBalance(),
      amount.doubleValue(), player.getCurrency(), currency);
    RequestValidator.validateGameRoundExists(gameRoundId, gameRoundService);

    GameRound gameRound = new GameRound(gameRoundId, round, playerId, gameId, Instant.now(), Instant.now(), false);
    GameTransaction gameTransaction = new GameTransaction(walletTxId, txId, gameRoundId, amount.doubleValue(), Type.BET, Instant.now());

    if (playerService.updateBalance(playerId, newBalance, Type.BET, gameRound, gameTransaction)) {
      BetResponse betResponse = createBetResponse(playerId, newBalance, currency, txId, walletTxId);

      LOGGER.info(String.format("Successfully created response:%n%-20s%n%-20s%n%-20s", gameRound, gameTransaction, betResponse));

      return betResponse;
    } else {
      WebException exception = new WebException(ExceptionCode.UNEXPECTED_ERROR);
      LOGGER.error(exception.getMessage());
      throw exception;
    }
  }

  private BetResponse createBetResponse(Integer playerId, Double balance, String currency,
                                        Integer originalTxId, String walletTxId) {
    BetDataResponse betDataResponse = new BetDataResponse();
    betDataResponse.setPlayerId(playerId);
    betDataResponse.setBalance(BigDecimal.valueOf(balance));
    betDataResponse.setCurrency(currency);
    betDataResponse.setOriginalTxId(originalTxId);
    betDataResponse.setWalletTxId(walletTxId);
    BetResponse betResponse = new BetResponse();
    betResponse.setStatus(ResponseStatus.SUCCESS.toString());
    betResponse.setData(betDataResponse);
    return betResponse;
  }


}
