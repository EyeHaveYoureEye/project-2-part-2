package tech.pm.edu.simlewallet.web.model;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import java.math.*;

@Data
public class WinRequest {

  private Integer playerId;
  private BigDecimal amount;
  private String currency;
  @JsonProperty("transaction")
  private WinTransactionRequest transaction;


}
