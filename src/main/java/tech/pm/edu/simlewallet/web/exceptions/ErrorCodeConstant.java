package tech.pm.edu.simlewallet.web.exceptions;

public interface ErrorCodeConstant {

  int CODE_801 = 801;
  int CODE_810 = 810;
  int CODE_820 = 820;
  int CODE_821 = 821;
  int CODE_831 = 831;
  int CODE_833 = 833;
  int CODE_840 = 840;


}
