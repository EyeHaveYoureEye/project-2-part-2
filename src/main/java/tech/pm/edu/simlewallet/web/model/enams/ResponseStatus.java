package tech.pm.edu.simlewallet.web.model.enams;

public enum ResponseStatus {

  SUCCESS, ERRORS;

  @Override
  public String toString() {
    return name().toLowerCase();
  }


}
