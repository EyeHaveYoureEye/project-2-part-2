package tech.pm.edu.simlewallet.web.controller;

import com.fasterxml.jackson.databind.*;
import tech.pm.edu.simlewallet.web.context.*;
import tech.pm.edu.simlewallet.web.exceptions.*;
import tech.pm.edu.simlewallet.web.exceptions.model.*;
import tech.pm.edu.simlewallet.web.exceptions.service.*;
import tech.pm.edu.simlewallet.web.model.*;
import tech.pm.edu.simlewallet.web.service.*;

import javax.servlet.annotation.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.stream.*;

@WebServlet("/api/game/win")
public class WinServlet extends HttpServlet {

  ObjectMapper objectMapper = new ObjectMapper();
  WinResponseService winResponseService;
  ErrorResponseService errorResponseService;

  @Override
  public void init() {
    winResponseService = ApplicationContextInjector.getWinResponseService();
    errorResponseService = ApplicationContextInjector.getErrorResponseService();
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    String winRequestData = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
    WinRequest winRequest = objectMapper.readValue(winRequestData, WinRequest.class);
    String response;
    try {
      WinResponse winResponse = winResponseService.getWinResponse(winRequest);
      response = objectMapper.writeValueAsString(winResponse);
      resp.getWriter().write(response);
    } catch (WebException webException) {
      ErrorResponseModel errorResponseModel = errorResponseService.getErrorResponseModel(webException);
      response = objectMapper.writeValueAsString(errorResponseModel);
      resp.getWriter().write(response);
    }
  }


}
