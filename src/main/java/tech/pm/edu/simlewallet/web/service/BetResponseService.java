package tech.pm.edu.simlewallet.web.service;

import tech.pm.edu.simlewallet.web.model.*;

public interface BetResponseService {

  BetResponse getBetResponse(BetRequest betRequest);


}
