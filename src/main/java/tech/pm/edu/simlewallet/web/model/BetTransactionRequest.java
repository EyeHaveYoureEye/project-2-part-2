package tech.pm.edu.simlewallet.web.model;

import lombok.*;

@Data
public class BetTransactionRequest {

  private String gameId;
  private Integer roundId;
  private Integer txId;
  private String walletTxId;

}
