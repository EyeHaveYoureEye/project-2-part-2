package tech.pm.edu.simlewallet.web.model;

import lombok.*;

import java.math.*;

@Data
public class WinDataResponse {

  private Integer playerId;
  private Integer originalTxId;
  private String walletTxId;
  private BigDecimal balance;
  private String currency;


}
