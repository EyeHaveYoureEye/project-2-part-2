package tech.pm.edu.simlewallet.web.context;

import tech.pm.edu.simlewallet.db.dao.*;
import tech.pm.edu.simlewallet.db.dao.impl.*;
import tech.pm.edu.simlewallet.db.service.*;
import tech.pm.edu.simlewallet.db.service.impl.*;
import tech.pm.edu.simlewallet.domain.service.*;
import tech.pm.edu.simlewallet.domain.service.impl.*;
import tech.pm.edu.simlewallet.web.exceptions.service.*;
import tech.pm.edu.simlewallet.web.exceptions.service.impl.*;
import tech.pm.edu.simlewallet.web.service.*;
import tech.pm.edu.simlewallet.web.service.impl.*;

public class ApplicationContextInjector {

  private static final GameDao GAME_DAO = new GameDaoImpl();
  private static final GameRoundDao GAME_ROUND_DAO = new GameRoundDaoImpl();
  private static final GameTransactionDao GAME_TRANSACTION_DAO = new GameTransactionDaoImpl();
  private static final PlayerDao PLAYER_DAO = new PlayerDaoImpl(GAME_ROUND_DAO, GAME_TRANSACTION_DAO);

  private static final GameEntityService GAME_ENTITY_SERVICE = new GameEntityServiceImpl(GAME_DAO);
  private static final GameRoundEntityService GAME_ROUND_ENTITY_SERVICE = new GameRoundEntityServiceImpl(GAME_ROUND_DAO);
  private static final GameTransactionEntityService GAME_TRANSACTION_ENTITY_SERVICE = new GameTransactionEntityServiceImpl(GAME_TRANSACTION_DAO);
  private static final PlayerEntityService PLAYER_ENTITY_SERVICE = new PlayerEntityServiceImpl(PLAYER_DAO);
  private static final GameRoundService GAME_ROUND_SERVICE = new GameRoundServiceImpl(GAME_ROUND_ENTITY_SERVICE);
  private static final GameService GAME_SERVICE = new GameServiceImpl(GAME_ENTITY_SERVICE);
  private static final GameTransactionService GAME_TRANSACTION_SERVICE = new GameTransactionServiceImpl(GAME_TRANSACTION_ENTITY_SERVICE);
  private static final PlayerService PLAYER_SERVICE = new PlayerServiceImpl(PLAYER_ENTITY_SERVICE);
  private static final BalanceResponseService BALANCE_RESPONSE_SERVICE = new BalanceResponseServiceImpl(PLAYER_SERVICE);
  private static final BetResponseService BET_RESPONSE_SERVICE = new BetResponseServiceImpl(PLAYER_SERVICE, GAME_SERVICE, GAME_ROUND_SERVICE);
  private static final WinResponseService WIN_RESPONSE_SERVICE = new WinResponseServiceImpl(PLAYER_SERVICE, GAME_ROUND_SERVICE, GAME_SERVICE, GAME_TRANSACTION_SERVICE);
  private static final ErrorResponseService ERROR_RESPONSE_SERVICE = new ErrorResponseServiceImpl();

  private ApplicationContextInjector() {
  }

  public static BalanceResponseService getBalanceResponseService() {
    return BALANCE_RESPONSE_SERVICE;
  }

  public static BetResponseService getBetResponseService() {
    return BET_RESPONSE_SERVICE;
  }

  public static WinResponseService getWinResponseService() {
    return WIN_RESPONSE_SERVICE;
  }

  public static ErrorResponseService getErrorResponseService() {
    return ERROR_RESPONSE_SERVICE;
  }


}
