package tech.pm.edu.simlewallet.web.exceptions.service.impl;

import tech.pm.edu.simlewallet.web.exceptions.*;
import tech.pm.edu.simlewallet.web.exceptions.model.*;
import tech.pm.edu.simlewallet.web.exceptions.service.*;
import tech.pm.edu.simlewallet.web.model.enams.*;

public class ErrorResponseServiceImpl implements ErrorResponseService {

  @Override
  public ErrorResponseModel getErrorResponseModel(WebException webException) {
    return webExceptionToErrorResponseModel(webException);
  }

  private ErrorResponseModel webExceptionToErrorResponseModel(WebException exception) {
    ErrorDetailsModel errorDetailsModel = new ErrorDetailsModel();
    errorDetailsModel.setCode(exception.getErrorCode());
    errorDetailsModel.setMessage(exception.getErrorMessage());
    ErrorResponseModel errorResponseModel = new ErrorResponseModel();
    errorResponseModel.setStatus(ResponseStatus.ERRORS.toString());
    errorResponseModel.setMessage(exception.getErrorMessage());
    errorResponseModel.addErrorDetailsToErrorsList(errorDetailsModel);
    return errorResponseModel;
  }


}
