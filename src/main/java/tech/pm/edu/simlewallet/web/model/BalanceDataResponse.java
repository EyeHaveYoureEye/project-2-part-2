package tech.pm.edu.simlewallet.web.model;

import lombok.*;

import java.math.*;

@Data
public class BalanceDataResponse {

  private Integer playerId;
  private BigDecimal balance;
  private String currency;


}

