package tech.pm.edu.simlewallet.db.model;

import lombok.*;
import tech.pm.edu.simlewallet.domain.model.enums.*;

@Value
public class GameEntity {

  String gameId;
  Status status;
  String gameName;


}
