package tech.pm.edu.simlewallet.db.service;

import tech.pm.edu.simlewallet.db.model.*;

import java.util.*;

public interface GameEntityService {

  Optional<GameEntity> getGameById(String gameId);


}
