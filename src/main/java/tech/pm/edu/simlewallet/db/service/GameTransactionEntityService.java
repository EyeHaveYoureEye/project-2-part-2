package tech.pm.edu.simlewallet.db.service;

import tech.pm.edu.simlewallet.db.model.*;

import java.util.*;

public interface GameTransactionEntityService {

  Optional<GameTransactionEntity> getGameTransactionById(String gameRoundId);

  Optional<List<GameTransactionEntity>> getListOfGameTransactionEntityByRoundId(String roundId);


}
