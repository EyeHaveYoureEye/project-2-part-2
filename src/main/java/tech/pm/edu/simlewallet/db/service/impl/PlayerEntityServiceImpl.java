package tech.pm.edu.simlewallet.db.service.impl;

import tech.pm.edu.simlewallet.db.dao.*;
import tech.pm.edu.simlewallet.db.mapper.*;
import tech.pm.edu.simlewallet.db.model.*;
import tech.pm.edu.simlewallet.db.service.*;
import tech.pm.edu.simlewallet.domain.model.*;
import tech.pm.edu.simlewallet.domain.model.enums.*;

import java.util.*;

public class PlayerEntityServiceImpl implements PlayerEntityService {

  private final PlayerDao playerDao;

  public PlayerEntityServiceImpl(PlayerDao playerDao) {
    this.playerDao = playerDao;
  }

  @Override
  public Optional<PlayerEntity> getPlayerById(Integer playerId) {
    return playerDao.findById(playerId);
  }

  @Override
  public boolean updateBalance(Integer playerId, Double newBalance, Type type, GameRound gameRound, GameTransaction gameTransaction) {
    return playerDao.updateBalance(playerId, newBalance, type,
      GameRoundEntityMapper.gameRoundToGameRoundEntity(gameRound),
      GameTransactionEntityMapper.gameTransactionToGameTransactionEntity(gameTransaction));
  }


}
