package tech.pm.edu.simlewallet.db.dao.impl;

import org.apache.log4j.*;
import tech.pm.edu.simlewallet.db.dao.*;
import tech.pm.edu.simlewallet.db.model.*;
import tech.pm.edu.simlewallet.domain.model.enums.*;

import java.sql.*;
import java.time.*;
import java.util.*;

public class GameTransactionDaoImpl extends AbstractGenericDao<GameTransactionEntity> implements GameTransactionDao {

  protected static final Logger LOGGER = Logger.getLogger(GameTransactionDaoImpl.class);

  private static final String READ_BY_GAME_ROUND_ID = "SELECT * FROM game_transaction WHERE round_id = ?";

  private static final String READ_ALL_BY_ROUND_ID = "SELECT * FROM game_transaction WHERE round_id = ?";

  private static final String INSERT = "INSERT INTO game_transaction VALUES (?, ?, ?, ?, ?, ?)";

  public GameTransactionDaoImpl() {
  }

  @Override
  public void create(GameTransactionEntity gameTransactionEntity, Connection connection) {
    insert(gameTransactionEntity, INSERT, connection);
  }

  @Override
  public Optional<GameTransactionEntity> findById(String gameRoundId) {
    return getElementByStringParam(gameRoundId, READ_BY_GAME_ROUND_ID);
  }

  @Override
  public Optional<List<GameTransactionEntity>> findAllByParam(String param) {
    return getList(READ_ALL_BY_ROUND_ID, param);
  }

  @Override
  public boolean update(GameTransactionEntity gameTransactionEntity) {
    LOGGER.error("Unsupported operation");
    return false;
  }

  @Override
  public boolean delete(String id) {
    LOGGER.error("Unsupported operation");
    return false;
  }

  @Override
  protected void setInsertElementProperties(PreparedStatement statement, GameTransactionEntity element) throws SQLException {
    statement.setString(1, element.getWalletTxId());
    statement.setInt(2, element.getTxId());
    statement.setString(3, element.getRoundId());
    statement.setDouble(4, element.getAmount());
    statement.setInt(5, element.getType().getValue());
    statement.setTimestamp(6, Timestamp.from(element.getTimestamp()));
  }

  @Override
  protected GameTransactionEntity parseToOneElement(ResultSet resultSet) throws SQLException {
    String walletTxId = resultSet.getString("wallet_tx_id");
    Integer txId = resultSet.getInt("tx_id");
    String roundId = resultSet.getString("round_id");
    Double amount = resultSet.getDouble("amount");
    Type typeId = toType(resultSet.getInt("type_id"));
    Instant timeStamp = resultSet.getTimestamp("time_stamp").toInstant();
    return new GameTransactionEntity(walletTxId, txId, roundId, amount, typeId, timeStamp);
  }

  private Type toType(int typeId) {
    if (Type.BET.getValue() == typeId) {
      return Type.BET;
    } else {
      return Type.WIN;
    }
  }


}
