package tech.pm.edu.simlewallet.db.service.impl;

import tech.pm.edu.simlewallet.db.dao.*;
import tech.pm.edu.simlewallet.db.model.*;
import tech.pm.edu.simlewallet.db.service.*;

import java.util.*;

public class GameEntityServiceImpl implements GameEntityService {

  private final GameDao gameDao;

  public GameEntityServiceImpl(GameDao gameDao) {
    this.gameDao = gameDao;
  }

  @Override
  public Optional<GameEntity> getGameById(String gameId) {
    return gameDao.findById(gameId);
  }


}