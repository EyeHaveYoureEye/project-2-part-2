package tech.pm.edu.simlewallet.db.dao.impl;

import org.apache.log4j.*;
import tech.pm.edu.simlewallet.db.dao.connection.*;

import java.sql.*;
import java.util.*;

public abstract class AbstractGenericDao<E> {

  protected static final Logger LOGGER = Logger.getLogger(AbstractGenericDao.class);

  protected void insert(E element, String query, Connection connection) {
    try (PreparedStatement statement = connection.prepareStatement(query)) {
      setInsertElementProperties(statement, element);
      statement.executeUpdate();
    } catch (SQLException e) {
      LOGGER.error(e);
    }
  }

  protected Optional<E> getElementByIntegerParam(Integer param, String query) {
    try (Connection connection = ConnectionPool.getConnection();
         PreparedStatement statement = connection.prepareStatement(query)) {
      statement.setInt(1, param);
      try (ResultSet resultSet = statement.executeQuery()) {
        if (resultSet.next()) {
          return Optional.of(parseToOneElement(resultSet));
        }
      }
    } catch (SQLException e) {
      LOGGER.error(e);
      return Optional.empty();
    }
    return Optional.empty();
  }

  protected Optional<E> getElementByStringParam(String param, String query) {
    try (Connection connection = ConnectionPool.getConnection();
         PreparedStatement statement = connection.prepareStatement(query)) {
      statement.setString(1, param);
      try (ResultSet resultSet = statement.executeQuery()) {
        if (resultSet.next()) {
          return Optional.of(parseToOneElement(resultSet));
        }
      }
    } catch (SQLException e) {
      LOGGER.error(e);
      return Optional.empty();
    }
    return Optional.empty();
  }

  protected Optional<List<E>> getList(String query, String param) {
    Optional<List<E>> list;
    try (Connection connection = ConnectionPool.getConnection();
         PreparedStatement statement = connection.prepareStatement(query)) {
      statement.setString(1, param);
      try (ResultSet resultSet = statement.executeQuery()) {
        list = parseAllElements(resultSet);
      }
    } catch (SQLException e) {
      LOGGER.error(e);
      return Optional.empty();
    }
    return list;
  }

  protected Optional<List<E>> parseAllElements(ResultSet resultSet) {
    List<E> elements = new ArrayList<>();
    try {
      while (resultSet.next()) {
        elements.add(parseToOneElement(resultSet));
      }
    } catch (SQLException e) {
      LOGGER.error(e);
      return Optional.empty();
    }
    return Optional.of(elements);
  }

  protected abstract void setInsertElementProperties(PreparedStatement statement, E element) throws SQLException;

  protected abstract E parseToOneElement(ResultSet resultSet) throws SQLException;


}
