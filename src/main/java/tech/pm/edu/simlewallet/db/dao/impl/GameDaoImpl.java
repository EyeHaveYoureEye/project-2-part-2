package tech.pm.edu.simlewallet.db.dao.impl;

import org.apache.log4j.*;
import tech.pm.edu.simlewallet.db.dao.*;
import tech.pm.edu.simlewallet.db.model.*;
import tech.pm.edu.simlewallet.domain.model.enums.*;

import java.sql.*;
import java.util.*;

public class GameDaoImpl extends AbstractGenericDao<GameEntity> implements GameDao {

  protected static final Logger LOGGER = Logger.getLogger(GameDaoImpl.class);

  private static final String READ_BY_ID = "SELECT * FROM Game WHERE game_id = ?";

  public GameDaoImpl() {
  }

  @Override
  public void create(GameEntity gameEntity, Connection connection) {
    LOGGER.error("Unsupported operation");
  }

  @Override
  public Optional<GameEntity> findById(String id) {
    return getElementByStringParam(id, READ_BY_ID);
  }

  @Override
  public Optional<List<GameEntity>> findAllByParam(String param) {
    LOGGER.error("Unsupported operation");
    return Optional.empty();
  }

  @Override
  public boolean update(GameEntity gameEntity) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean delete(String id) {
    throw new UnsupportedOperationException();
  }

  @Override
  protected void setInsertElementProperties(PreparedStatement statement, GameEntity element) throws SQLException {
    statement.setString(1, element.getGameId());
    statement.setInt(2, element.getStatus().getValue());
    statement.setString(3, element.getGameName());
  }

  @Override
  protected GameEntity parseToOneElement(ResultSet resultSet) throws SQLException {
    String game_id = resultSet.getString("game_id");
    Status statusId = toStatus(resultSet.getInt("status_id"));
    String gameName = resultSet.getString("full_game_name");
    return new GameEntity(game_id, statusId, gameName);
  }

  private Status toStatus(int statusId) {
    if (Status.ACTIVE.getValue() == statusId) {
      return Status.ACTIVE;
    } else {
      return Status.INACTIVE;
    }
  }


}
