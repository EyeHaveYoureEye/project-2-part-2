package tech.pm.edu.simlewallet.db.service.impl;

import tech.pm.edu.simlewallet.db.dao.*;
import tech.pm.edu.simlewallet.db.model.*;
import tech.pm.edu.simlewallet.db.service.*;

import java.util.*;

public class GameTransactionEntityServiceImpl implements GameTransactionEntityService {

  private final GameTransactionDao gameTransactionDao;

  public GameTransactionEntityServiceImpl(GameTransactionDao gameTransactionDao) {
    this.gameTransactionDao = gameTransactionDao;

  }

  @Override
  public Optional<GameTransactionEntity> getGameTransactionById(String gameRoundId) {
    return gameTransactionDao.findById(gameRoundId);
  }

  @Override
  public Optional<List<GameTransactionEntity>> getListOfGameTransactionEntityByRoundId(String roundId) {
    return gameTransactionDao.findAllByParam(roundId);
  }


}
