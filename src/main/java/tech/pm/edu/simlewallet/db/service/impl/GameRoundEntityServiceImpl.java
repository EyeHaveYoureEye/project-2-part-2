package tech.pm.edu.simlewallet.db.service.impl;

import tech.pm.edu.simlewallet.db.dao.*;
import tech.pm.edu.simlewallet.db.model.*;
import tech.pm.edu.simlewallet.db.service.*;

import java.util.*;

public class GameRoundEntityServiceImpl implements GameRoundEntityService {

  private final GameRoundDao gameRoundDao;

  public GameRoundEntityServiceImpl(GameRoundDao gameRoundDao) {
    this.gameRoundDao = gameRoundDao;
  }

  @Override
  public Optional<GameRoundEntity> getGameRoundByRoundId(String gameRoundId) {
    return gameRoundDao.findById(gameRoundId);
  }


}
