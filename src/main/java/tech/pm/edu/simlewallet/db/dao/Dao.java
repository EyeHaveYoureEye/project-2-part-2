package tech.pm.edu.simlewallet.db.dao;

import java.sql.*;
import java.util.*;

public interface Dao<ID, T> {

  void create(T entity, Connection connection);

  Optional<T> findById(ID id);

  Optional<List<T>> findAllByParam(String param);

  boolean update(T t);

  boolean delete(ID id);


}
