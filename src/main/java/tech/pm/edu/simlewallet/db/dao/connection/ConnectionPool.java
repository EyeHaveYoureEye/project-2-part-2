package tech.pm.edu.simlewallet.db.dao.connection;

import org.apache.commons.dbcp.*;
import org.apache.log4j.*;

import javax.sql.*;
import java.sql.*;
import java.util.*;

public class ConnectionPool {

  private static final Logger LOGGER = Logger.getLogger(ConnectionPool.class);
  private static final ConnectionPool INSTANCE = new ConnectionPool();
  private static DataSource dataSource;

  private ConnectionPool() {
    try {
      ResourceBundle dbConfig = ResourceBundle.getBundle("db");
      BasicDataSource basicDataSource = new BasicDataSource();
      basicDataSource.setUrl(dbConfig.getString("db.connection.url"));
      basicDataSource.setDriverClassName(dbConfig.getString("db.connection.driver"));
      basicDataSource.setUsername(dbConfig.getString("db.connection.username"));
      basicDataSource.setPassword(dbConfig.getString("db.connection.password"));
      basicDataSource.setMinIdle(Integer.parseInt(dbConfig.getString("db.connection.minIdle")));
      basicDataSource.setMaxIdle(Integer.parseInt(dbConfig.getString("db.connection.maxIdle")));
      basicDataSource.setMaxActive(Integer.parseInt(dbConfig.getString("db.connection.maxActive")));
      basicDataSource.setMaxOpenPreparedStatements(Integer.parseInt(dbConfig.getString("db.connection.maxOpenPreparedStatements")));
      dataSource = basicDataSource;
    } catch (Exception e) {
      LOGGER.error(e);
    }
  }

  private Connection createConnection() throws SQLException {
    try {
      return dataSource.getConnection();
    } catch (SQLException e) {
      LOGGER.error(e);
      throw e;
    }
  }

  public static Connection getConnection() throws SQLException {
    return INSTANCE.createConnection();
  }


}
