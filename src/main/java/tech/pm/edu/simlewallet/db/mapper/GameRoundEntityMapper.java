package tech.pm.edu.simlewallet.db.mapper;

import tech.pm.edu.simlewallet.db.model.*;
import tech.pm.edu.simlewallet.domain.model.*;

public class GameRoundEntityMapper {

  private GameRoundEntityMapper() {
  }

  public static GameRoundEntity gameRoundToGameRoundEntity(GameRound gameRound) {
    if (gameRound != null) {
      return new GameRoundEntity(
        gameRound.getGameRoundId(),
        gameRound.getGameRound(),
        gameRound.getPlayerId(),
        gameRound.getGameId(),
        gameRound.getStartTime(),
        gameRound.getEndTime(),
        gameRound.getIsFinished()
      );
    } else {
      return null;
    }
  }


}
