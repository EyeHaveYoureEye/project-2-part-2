package tech.pm.edu.simlewallet.db.dao.impl;

import org.apache.log4j.*;
import tech.pm.edu.simlewallet.db.dao.*;
import tech.pm.edu.simlewallet.db.model.*;

import java.sql.*;
import java.time.*;
import java.util.*;

public class GameRoundDaoImpl extends AbstractGenericDao<GameRoundEntity> implements GameRoundDao {

  protected static final Logger LOGGER = Logger.getLogger(GameRoundDaoImpl.class);

  private static final String READ_BY_ID = "SELECT * FROM game_round WHERE id = ?";

  private static final String INSERT = "INSERT INTO game_round VALUES (?, ?, ?, ?, ?, ?, ?)";

  public GameRoundDaoImpl() {
  }

  @Override
  public void create(GameRoundEntity gameRoundEntity, Connection connection) {
    insert(gameRoundEntity, INSERT, connection);
  }

  @Override
  public Optional<GameRoundEntity> findById(String id) {
    return getElementByStringParam(id, READ_BY_ID);
  }

  @Override
  public Optional<List<GameRoundEntity>> findAllByParam(String param) {
    LOGGER.error("Unsupported operation");
    return Optional.empty();
  }

  @Override
  public boolean update(GameRoundEntity gameRoundEntity) {
    LOGGER.error("Unsupported operation");
    return false;
  }

  @Override
  public boolean delete(String integer) {
    LOGGER.error("Unsupported operation");
    return false;
  }

  @Override
  protected void setInsertElementProperties(PreparedStatement statement, GameRoundEntity element) throws SQLException {
    statement.setString(1, element.getGameRoundId());
    statement.setInt(2, element.getGameRound());
    statement.setInt(3, element.getPlayerId());
    statement.setString(4, element.getGameId());
    statement.setTimestamp(5, Timestamp.from(element.getStartTime()));
    statement.setTimestamp(6, Timestamp.from(element.getEndTime()));
    statement.setBoolean(7, element.getIsFinished());
  }

  @Override
  protected GameRoundEntity parseToOneElement(ResultSet resultSet) throws SQLException {
    String id = resultSet.getString("id");
    Integer gameRoundId = resultSet.getInt("game_round_id");
    Integer playerId = resultSet.getInt("player_id");
    String gameId = resultSet.getString("game_id");
    Instant startTime = resultSet.getTimestamp("start_time").toInstant();
    Instant endTime = resultSet.getTimestamp("end_time").toInstant();
    boolean isFinished = resultSet.getBoolean("is_finished");
    return new GameRoundEntity(id, gameRoundId, playerId, gameId, startTime, endTime, isFinished);
  }


}
