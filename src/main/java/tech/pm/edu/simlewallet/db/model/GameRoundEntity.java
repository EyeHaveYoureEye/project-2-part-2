package tech.pm.edu.simlewallet.db.model;

import lombok.*;

import java.time.*;

@Value
public class GameRoundEntity {

  String gameRoundId;
  Integer gameRound;
  Integer playerId;
  String gameId;
  Instant startTime;
  Instant endTime;
  Boolean isFinished;


}
