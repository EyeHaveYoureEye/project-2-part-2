package tech.pm.edu.simlewallet.db.service;

import tech.pm.edu.simlewallet.db.model.*;
import tech.pm.edu.simlewallet.domain.model.*;
import tech.pm.edu.simlewallet.domain.model.enums.*;

import java.util.*;

public interface PlayerEntityService {

  Optional<PlayerEntity> getPlayerById(Integer playerId);

  boolean updateBalance(Integer playerId, Double newBalance, Type type, GameRound gameRound, GameTransaction gameTransaction);


}
