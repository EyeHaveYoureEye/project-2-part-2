package tech.pm.edu.simlewallet.db.dao;

import tech.pm.edu.simlewallet.db.model.*;
import tech.pm.edu.simlewallet.domain.model.enums.*;

public interface PlayerDao extends Dao<Integer, PlayerEntity> {

  boolean updateBalance(Integer playerId, Double newBalance, Type type,
                        GameRoundEntity gameRoundEntity, GameTransactionEntity gameTransactionEntity);


}
