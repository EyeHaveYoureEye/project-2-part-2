package tech.pm.edu.simlewallet.db.mapper;

import tech.pm.edu.simlewallet.db.model.*;
import tech.pm.edu.simlewallet.domain.model.*;

public class GameTransactionEntityMapper {

  private GameTransactionEntityMapper() {
  }

  public static GameTransactionEntity gameTransactionToGameTransactionEntity(GameTransaction gameTransaction) {
    if (gameTransaction != null) {
      return new GameTransactionEntity(
        gameTransaction.getWalletTxId(),
        gameTransaction.getTxId(),
        gameTransaction.getRoundId(),
        gameTransaction.getAmount(),
        gameTransaction.getType(),
        gameTransaction.getTimestamp()
      );
    } else {
      return null;
    }
  }


}
