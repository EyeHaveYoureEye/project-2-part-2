package tech.pm.edu.simlewallet.db.service;

import tech.pm.edu.simlewallet.db.model.*;

import java.util.*;

public interface GameRoundEntityService {

  Optional<GameRoundEntity> getGameRoundByRoundId(String gameRoundId);


}
