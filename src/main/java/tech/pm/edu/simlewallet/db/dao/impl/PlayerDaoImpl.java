package tech.pm.edu.simlewallet.db.dao.impl;

import org.apache.log4j.*;
import tech.pm.edu.simlewallet.db.dao.*;
import tech.pm.edu.simlewallet.db.dao.connection.*;
import tech.pm.edu.simlewallet.db.model.*;
import tech.pm.edu.simlewallet.domain.model.enums.*;

import java.sql.*;
import java.time.*;
import java.util.*;

public class PlayerDaoImpl extends AbstractGenericDao<PlayerEntity> implements PlayerDao {

  private final GameRoundDao gameRoundDao;
  private final GameTransactionDao gameTransactionDao;

  protected static final Logger LOGGER = Logger.getLogger(PlayerDaoImpl.class);

  private static final String READ_BY_ID = "SELECT * FROM player WHERE id = ?";

  private static final String UPDATE_BALANCE = "UPDATE player SET balance = ? WHERE id = ?";

  public PlayerDaoImpl(GameRoundDao gameRoundDao, GameTransactionDao gameTransactionDao) {
    this.gameRoundDao = gameRoundDao;
    this.gameTransactionDao = gameTransactionDao;
  }

  @Override
  public void create(PlayerEntity playerEntity, Connection connection) {
    LOGGER.error("Unsupported operation");
  }

  @Override
  public Optional<PlayerEntity> findById(Integer id) {
    return getElementByIntegerParam(id, READ_BY_ID);
  }

  @Override
  public Optional<List<PlayerEntity>> findAllByParam(String param) {
    LOGGER.error("Unsupported operation");
    return Optional.empty();
  }

  @Override
  public boolean update(PlayerEntity playerEntity) {
    LOGGER.error("Unsupported operation");
    return false;
  }

  @Override
  public boolean delete(Integer integer) {
    LOGGER.error("Unsupported operation");
    return false;
  }

  // update player balance:
  // if type is bet - update player balance, create a game round and create a game transaction in one transaction
  // if type is win - update player balance, set game round is finished and create a game transaction in one transaction
  // rollback in case of error
  @Override
  public boolean updateBalance(Integer playerId, Double newBalance, Type type,
                               GameRoundEntity gameRoundEntity, GameTransactionEntity gameTransactionEntity) {
    try (Connection connection = ConnectionPool.getConnection()) {
      connection.setAutoCommit(false);
      Savepoint savepoint = connection.setSavepoint("Savepoint");
      try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_BALANCE)) {
        preparedStatement.setDouble(1, newBalance);
        preparedStatement.setInt(2, playerId);
        preparedStatement.executeUpdate();
        if (type.equals(Type.BET)) {
          gameRoundDao.create(gameRoundEntity, connection);
        }
        gameTransactionDao.create(gameTransactionEntity, connection);
        connection.commit();
        connection.setAutoCommit(true);
        return true;
      } catch (SQLException exception) {
        LOGGER.error(exception);
        connection.rollback(savepoint);
        connection.setAutoCommit(true);
        return false;
      }
    } catch (SQLException exception) {
      LOGGER.error(exception);
      return false;
    }
  }

  @Override
  protected void setInsertElementProperties(PreparedStatement statement, PlayerEntity element) throws SQLException {
    statement.setTimestamp(1, Timestamp.from(element.getRegistrationDate()));
    statement.setInt(2, element.getStatus().getValue());
    statement.setInt(3, element.getCountryId());
    statement.setDouble(4, element.getBalance());
    statement.setString(5, element.getCurrency());
  }

  @Override
  protected PlayerEntity parseToOneElement(ResultSet resultSet) throws SQLException {
    Integer id = resultSet.getInt("id");
    Instant registrationDate = resultSet.getTimestamp("registration_date").toInstant();
    Status statusId = toStatus(resultSet.getInt("status_id"));
    Integer countryId = resultSet.getInt("country_id");
    Double balance = resultSet.getDouble("balance");
    String currency = resultSet.getString("currency");
    return new PlayerEntity(id, registrationDate, statusId, countryId, balance, currency);
  }

  private Status toStatus(int statusId) {
    if (Status.ACTIVE.getValue() == statusId) {
      return Status.ACTIVE;
    } else {
      return Status.INACTIVE;
    }
  }


}
