INSERT INTO status
VALUES (DEFAULT, 'Active',
        ' Player: means that the player has the right to place bets.
         Game: means that the game is currently in progress.');
INSERT INTO status
VALUES (DEFAULT, 'Inactive',
        ' Player: Player is banned. Game: the game is over, or has not been started yet.');

INSERT INTO transaction_type (type_transaction)
VALUES ('Bet');
INSERT INTO transaction_type (type_transaction)
VALUES ('Win');

INSERT INTO game(game_id, status_id, full_game_name)
VALUES ('Lucky Blue', 1, 'Lucky Blue PM');
INSERT INTO game(game_id, status_id, full_game_name)
VALUES ('West Town', 2, 'West Town 1xBet');
INSERT INTO game(game_id, status_id, full_game_name)
VALUES ('Burning Wins', 1, 'Burning Wins PM');

INSERT INTO country (country_name, country_code)
VALUES ('Afghanistan', 'AF');
INSERT INTO country (country_name, country_code)
VALUES ('Aland Islands', 'AX');
INSERT INTO country (country_name, country_code)
VALUES ('Albania', 'AL');
INSERT INTO country (country_name, country_code)
VALUES ('Algeria', 'DZ');
INSERT INTO country (country_name, country_code)
VALUES ('American Samoa', 'AS');
INSERT INTO country (country_name, country_code)
VALUES ('Andorra', 'AD');
INSERT INTO country (country_name, country_code)
VALUES ('Angola', 'AO');
INSERT INTO country (country_name, country_code)
VALUES ('Anguilla', 'AI');
INSERT INTO country (country_name, country_code)
VALUES ('Antarctica', 'AQ');
INSERT INTO country (country_name, country_code)
VALUES ('Antigua and Barbuda', 'AG');
INSERT INTO country (country_name, country_code)
VALUES ('Argentina', 'AR');
INSERT INTO country (country_name, country_code)
VALUES ('Armenia', 'AM');
INSERT INTO country (country_name, country_code)
VALUES ('Aruba', 'AW');
INSERT INTO country (country_name, country_code)
VALUES ('Australia', 'AU');
INSERT INTO country (country_name, country_code)
VALUES ('Austria', 'AT');
INSERT INTO country (country_name, country_code)
VALUES ('Azerbaijan', 'AZ');

INSERT INTO country (country_name, country_code)
VALUES ('Bangladesh', 'BD');
INSERT INTO country (country_name, country_code)
VALUES ('Barbados', 'BB');
INSERT INTO country (country_name, country_code)
VALUES ('Belarus', 'BY');
INSERT INTO country (country_name, country_code)
VALUES ('Belgium', 'BE');
INSERT INTO country (country_name, country_code)
VALUES ('Bolivia', 'BO');
INSERT INTO country (country_name, country_code)
VALUES ('Bosnia and Herzegovina', 'BA');
INSERT INTO country (country_name, country_code)
VALUES ('Botswana', 'BW');
INSERT INTO country (country_name, country_code)
VALUES ('Brazil', 'BR');
INSERT INTO country (country_name, country_code)
VALUES ('British Virgin Islands', 'VG');
INSERT INTO country (country_name, country_code)
VALUES ('British Indian Ocean Territory', 'IO');
INSERT INTO country (country_name, country_code)
VALUES ('Bulgaria', 'BG');

INSERT INTO country (country_name, country_code)
VALUES ('Canada', 'CA');
INSERT INTO country (country_name, country_code)
VALUES ('Cayman Islands', 'KY');
INSERT INTO country (country_name, country_code)
VALUES ('Central African Republic', 'CF');
INSERT INTO country (country_name, country_code)
VALUES ('China', 'CN');
INSERT INTO country (country_name, country_code)
VALUES ('Hong Kong', 'HK');
INSERT INTO country (country_name, country_code)
VALUES ('Colombia', 'CO');
INSERT INTO country (country_name, country_code)
VALUES ('Congo', 'CG');
INSERT INTO country (country_name, country_code)
VALUES ('Costa Rica', 'CR');
INSERT INTO country (country_name, country_code)
VALUES ('Cuba', 'CU');
INSERT INTO country (country_name, country_code)
VALUES ('Czech Republic', 'CZ');

INSERT INTO country (country_name, country_code)
VALUES ('Dominica', 'DM');

INSERT INTO country (country_name, country_code)
VALUES ('Ecuador', 'EC');
INSERT INTO country (country_name, country_code)
VALUES ('Egypt', 'EG');
INSERT INTO country (country_name, country_code)
VALUES ('Estonia', 'EE');
INSERT INTO country (country_name, country_code)
VALUES ('Ethiopia', 'ET');

INSERT INTO country (country_name, country_code)
VALUES ('Fiji', 'FJ');
INSERT INTO country (country_name, country_code)
VALUES ('Finland', 'FI');
INSERT INTO country (country_name, country_code)
VALUES ('	France', 'FR');

INSERT INTO country (country_name, country_code)
VALUES ('Germany', 'DE');
INSERT INTO country (country_name, country_code)
VALUES ('Ghana', 'GH');
INSERT INTO country (country_name, country_code)
VALUES ('Greenland', 'GL');
INSERT INTO country (country_name, country_code)
VALUES ('Guyana', 'GN');

INSERT INTO country (country_name, country_code)
VALUES ('Haiti', 'HT');
INSERT INTO country (country_name, country_code)
VALUES ('Honduras', 'HN');

INSERT INTO country (country_name, country_code)
VALUES ('Iceland', 'IS');
INSERT INTO country (country_name, country_code)
VALUES ('India', 'IN');
INSERT INTO country (country_name, country_code)
VALUES ('Indonesia', 'ID');
INSERT INTO country (country_name, country_code)
VALUES ('Iraq', 'IQ');
INSERT INTO country (country_name, country_code)
VALUES ('Israel', 'IL');
INSERT INTO country (country_name, country_code)
VALUES ('Italy', 'IT');

INSERT INTO country (country_name, country_code)
VALUES ('Japan', 'JP');

INSERT INTO country (country_name, country_code)
VALUES ('Kazakhstan', 'KZ');
INSERT INTO country (country_name, country_code)
VALUES ('Korea ', 'KR');
INSERT INTO country (country_name, country_code)
VALUES ('Kyrgyzstan', 'KG');

INSERT INTO country (country_name, country_code)
VALUES ('Luxembourg', 'LU');

INSERT INTO country (country_name, country_code)
VALUES ('Madagascar', 'MG');
INSERT INTO country (country_name, country_code)
VALUES ('Malaysia', 'MY');

INSERT INTO country (country_name, country_code)
VALUES ('Netherlands', 'NL');
INSERT INTO country (country_name, country_code)
VALUES ('Niger', 'NE');
INSERT INTO country (country_name, country_code)
VALUES ('Nigeria', 'NG');

INSERT INTO country (country_name, country_code)
VALUES ('Poland', 'PL');

INSERT INTO country (country_name, country_code)
VALUES ('Russian Federation', 'RU');
INSERT INTO country (country_name, country_code)
VALUES ('Romania', 'RO');

INSERT INTO country (country_name, country_code)
VALUES ('Serbia', 'RS');

INSERT INTO country (country_name, country_code)
VALUES ('Tajikistan', 'TJ');
INSERT INTO country (country_name, country_code)
VALUES ('Turkey', 'TR');

INSERT INTO country (country_name, country_code)
VALUES ('Ukraine', 'UA');
INSERT INTO country (country_name, country_code)
VALUES ('United Kingdom', 'GB');

INSERT INTO country (country_name, country_code)
VALUES ('United States of America', 'US');
INSERT INTO country (country_name, country_code)
VALUES ('Uzbekistan', 'UZ');

INSERT INTO country (country_name, country_code)
VALUES ('Viet Nam', 'VN');
INSERT INTO country (country_name, country_code)
VALUES ('Uzbekistan', 'UZ');



INSERT INTO player(registration_date, status_id, country_id, balance, currency)
VALUES ('1999-01-08 04:05:06', 1, 1, 1000, 'UAH');
INSERT INTO player(registration_date, status_id, country_id, balance, currency)
VALUES ('1999-01-08 04:05:06', 1, 2, 1000, 'UAH');
INSERT INTO player(registration_date, status_id, country_id, balance, currency)
VALUES ('1999-01-08 04:05:06', 1, 3, 1000, 'UAH');
INSERT INTO player(registration_date, status_id, country_id, balance, currency)
VALUES ('1999-01-08 04:05:06', 1, 4, 1000, 'UAH');
INSERT INTO player(registration_date, status_id, country_id, balance, currency)
VALUES ('1999-01-08 04:05:06', 1, 5, 1000, 'UAH');
INSERT INTO player(registration_date, status_id, country_id, balance, currency)
VALUES ('1999-01-08 04:05:06', 1, 6, 1000, 'UAH');
INSERT INTO player(registration_date, status_id, country_id, balance, currency)
VALUES ('1999-01-08 04:05:06', 1, 7, 1000, 'UAH');
INSERT INTO player(registration_date, status_id, country_id, balance, currency)
VALUES ('1999-01-08 04:05:06', 1, 8, 1000, 'UAH');